import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { ItemTable } from './item-table';

const url = 'https://desafio-it-server.herokuapp.com/lancamentos';

@Injectable({
  providedIn: 'root'
})
export class HistoricoFaturaService {
  constructor(private http: HttpClient) { }

  public getLancamentos() :  Observable<ItemTable[]> {
    return this.http.get<ItemTable[]>(url);
    }

}
