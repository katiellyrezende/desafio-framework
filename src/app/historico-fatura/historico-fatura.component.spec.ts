import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricoFaturaComponent } from './historico-fatura.component';

describe('HistoricoFaturaComponent', () => {
  let component: HistoricoFaturaComponent;
  let fixture: ComponentFixture<HistoricoFaturaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoricoFaturaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricoFaturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
