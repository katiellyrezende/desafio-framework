import { Component, OnInit } from '@angular/core';
import { HistoricoFaturaService } from './historico-fatura.service';
import { ItemTable } from './item-table';

@Component({
  selector: 'app-historico-fatura',
  templateUrl: './historico-fatura.component.html',
  styleUrls: ['./historico-fatura.component.css']
})
export class HistoricoFaturaComponent implements OnInit {

  public tableItems: Array<ItemTable> = [];
  public consolidated=[];

  constructor(private historicoFaturaService: HistoricoFaturaService) { }

  ngOnInit() {
    this.getInformation();
  }

  private getInformation(): void {
      this.historicoFaturaService.getLancamentos()
      .subscribe(response => {
        this.setTableItem(response);
      });
  }

  private setTableItem(response: Array<ItemTable> = []): void {
    this.tableItems.length = 0;

    response.forEach((item) => {
      this.tableItems.push(item);
        item.cMes_lancamento = this.returnMonth((item.mes_lancamento)) ;
        item.cCategoria = this.returnCategory(item.categoria);
    });

    this.tableItems.sort(function (a, b){
      return  a.mes_lancamento<b.mes_lancamento?-1:1;
    });

    this.returnDados(this.tableItems);
  }

  private returnDados(tableItems): void {
    let aux= 0;
    
    tableItems.forEach((item) => {
      if (item.mes_lancamento !== aux) {
        this.consolidated.push({
          month: item.mes_lancamento,
          cMonth: this.returnMonth((item.mes_lancamento)),
          value: item.valor
        });
      } else {
        this.consolidated.forEach((consolidated) => {
          if (consolidated.month === aux) {
              consolidated.value += parseFloat(item.valor);
          }
        });
      }
      aux = item.mes_lancamento;
    });
  } 

  private returnMonth(month: number): string {
    const months = {
      1: "Janeiro",
      2: "Fevereiro",
      3: "Março",
      4: "Abril",
      5: "Maio",
      6: "Junho",
      7: "Julho",
      8: "Agosto",
      9: "Setembro",
      10: "Outubro",
      11: "Novembro",
      12: "Dezembro"
    }          
  return months[month];
  }

  private returnCategory(category: number): string {
    const categories = {
      1: "Transporte",
      2: "Compras Online",
      3: "Saúde e Beleza",
      4: "Serviços Automotivos",
      5: "Restaurantes",
      6: "Super Mercados",
    }          
  return categories[category];
  }

}