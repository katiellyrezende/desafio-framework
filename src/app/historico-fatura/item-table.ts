export class ItemTable{
    public id: number;
    public valor: number;
    public origem: string;
    public categoria: number;
    public mes_lancamento: number;
    public cMes_lancamento?: string;
    public cCategoria?: string;
}