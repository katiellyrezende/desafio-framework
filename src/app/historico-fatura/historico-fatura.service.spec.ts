import { TestBed } from '@angular/core/testing';

import { HistoricoFaturaService } from './historico-fatura.service';

describe('HistoricoFaturaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HistoricoFaturaService = TestBed.get(HistoricoFaturaService);
    expect(service).toBeTruthy();
  });
});
