import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HistoricoFaturaService } from './historico-fatura/historico-fatura.service';


const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    HistoricoFaturaService
  ]
})
export class AppRoutingModule { }
